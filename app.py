# -*- coding: utf-8 -*-

import re
import json
import os
import sys


CONFIG_FILE    = "./app_configuration.json"
TOLERATES_FILE = "./tolerates.cfg"
OUTPUT_FILE    = "clair-output.txt"

# The script should receive one argument: the docker image to scan
if (len(sys.argv) < 2):
    print("ERROR: Must receive one argument.")
    print("       Script usage: {} REGISTRY_ADDR/ImageToAnalyze".format(sys.argv[0]))
    exit(1)

image = sys.argv[1]

# Check files exist
if not os.path.isfile(CONFIG_FILE):
    print("ERROR: Configuration file " + CONFIG_FILE + " not found.")
    exit(1)
if not os.path.isfile(TOLERATES_FILE):
    print("ERROR: Tolerates file " + TOLERATES_FILE + " not found.")
    exit(1)

with open(TOLERATES_FILE,"r") as tolerates:
    tolerate=tolerates.readlines()

with open(CONFIG_FILE, "r") as config_f:
    config = json.load(config_f)

# klar_bin    = config['KLAR_BINARY']
clair_addr  = config['CLAIR_ADDR']
levels      = config['LEVELS_OF_VULNERABILITIES']
ignore      = config['QUANTITY_TO_IGNORE']

klar_command = "sudo CLAIR_ADDR={} REGISTRY_INSECURE=true klar {} > {} 2>&1".format(clair_addr, image, OUTPUT_FILE)

# Run clair analysis
klar = os.system(klar_command)
exit_code = os.WEXITSTATUS(klar)

if (exit_code != 0 and exit_code != 1):
    print("ERROR: Klar execution failed.")
    exit(1)

# Check clair output
with open (OUTPUT_FILE,"r") as clair:
    data=clair.readlines()

# Counters
counters = {}
for level in levels:
    counters.update({level: 0})

# Total of errors founds in analysis
for line in data:
    for level in levels:
        if line.startswith(level.capitalize() + ":"):
            counters[level] = int(line.strip("\n").strip(level.capitalize() + ": "))

lista_cve=[]

# Save all CVE in array
for line in data:
    if line.startswith('CVE'):
        lista_cve.append(line)

# For each tolerate in the file if this are present in array substract one of the counters and remove this.
for tol in tolerate:
    for cve in lista_cve:
        for level in levels:
            if cve.startswith(tol.strip("\n")):
                if cve.endswith("[{}]\n".format(level.capitalize())):
                    counters[level] -= 1
                lista_cve.remove(cve)

flag = True

# If some condition is not passed the test will fail
for level in counters:
    if counters[level] > ignore[level] and ignore[level] >= 0:
        flag = False

# If test fails, show the cve
if flag == True:
    print("Image is secure enough!")
    exit(0)
else:
    print("Image is not secure enough, list of vulnerabilities found is shown below\n")
    for cve in lista_cve:
        print(cve.strip("\n").strip(":"))
    exit(1)
