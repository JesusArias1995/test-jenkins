#!/bin/bash

# Root is needed in order to run the commands
if [[ "$(id -u)" != "0" ]]; then
   echo "Error: this script must be run as root!" 1>&2
   exit 1
fi

# Check go
GO_VERSION="1.12.6"

if [[ ! "$(command -v go)" ]]; then
    GO_PACKAGE="go${GO_VERSION}.linux-amd64.tar.gz" 
    GO_URL="https://dl.google.com/go/${GO_PACKAGE}"
    wget ${GO_URL}
    if [[ ! $? ]]; then
        echo "Error downloading go package from  ${GO_URL}, exiting..."
        exit 1
    fi
    tar -C /usr/local -xzf ${GO_PACKAGE}
    if [[ ! $? ]]; then
        echo "Error extracting ${GO_PACKAGE}, exiting..."
        exit 1
    fi
    cp /usr/local/go/bin/* /usr/bin/
    echo "Successfully installed go ${GO_VERSION}"
fi


# Check Klar

if [[ ! "$(command -v klar)" ]]; then
    /root/go/bin/klar > /dev/null 2>&1
    if [[ ! "$(command -v /root/go/bin/klar)" ]]; then
        go get github.com/optiopay/klar
        if [[ ! $? ]]; then
            echo "Error running 'go get', exiting..."
            exit 1
        fi
    fi
    cp /root/go/bin/klar /usr/bin/klar
    echo "Successfully installed Klar"
fi