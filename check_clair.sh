#!/bin/bash

CONFIG_FILE=app_configuration.json

CLAIR_HEALTH_ADDR=$(sed -n 's|.*"CLAIR_HEALTH_ADDR": "\([^"]*\)".*|\1|p' ${CONFIG_FILE})

if [[ "$(curl --write-out %{http_code} --silent --output /dev/null ${CLAIR_HEALTH_ADDR})" == "200" ]]; then
    echo "Clair ok"
fi

