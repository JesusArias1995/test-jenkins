#!/usr/bin/python

import os
import sys


# Get the OS where the app is running
def whereIAm():
    me = os.popen("cat /etc/os-release |grep '^ID='").read().split('ID=')
    return me[1].strip()


# Check if the pakcage have dependencies in the blacklist (Debian, Ubuntu)
def checkDebianBlacklist(package,blacklist):

    coincidences = False

    # Get the package dependencies
    packinfo = os.popen("apt-cache depends " + package).read()

    # Check if the package exists
    if package not in packinfo:
        sys.exit(1)

    try:
    # Get the name of the dependencies
        dependencies = packinfo.split('Depends:')
        del dependencies[0]
        del dependencies[len(dependencies)-1]
        lastdependency = dependencies[len(dependencies)-1].split('Suggests:')[0]
        depslist = [lastdependency.strip()]

        for i in dependencies:
            depslist.append(i.strip())


        # Check if the dependencies are in the blacklist
        for i in depslist:
            for j in blacklist:
                if j in i:
                   print("This dependence is not allowed " + i)
                   coincidences = True
    except IndexError:
        packinfo = packinfo.split(">")[0]
        packinfo = packinfo.split("<")[1]
        if packinfo in blacklist:
           coincidences = True

    # If there are dependencies in the blacklist, break the pipeline.
    if coincidences:
        print "Package {} not allowed".format(package)
        sys.exit(1)


# Check if the pakcage have dependencies in the blacklist (SUSE)
def checkSUSEBlacklist(package, blacklist):

    coincidences = False

    # Get the package dependecies
    packinfo = os.popen("zypper info --requires " + package).read()
    dependencies = packinfo.split(']')

    # Check the package exists
    try:
        splitdeps = dependencies[1].split('\n')
    except IndexError:
        print "Package not found"
        sys.exit(1)

    # Get the dependencies name
    depslist = []

    for i in splitdeps:
        depslist.append(i.strip())

    # Check if the dependencies are in the blacklist
    for i in depslist:
        for j in blacklist:
            if j in i:
                print("This dependence is not allowed " + i)
                coincidences = True

    # If there are dependencies in the blacklist, break the pipeline.
    if coincidences:
        sys.exit(1)

# Check if the pakcage have dependencies in the blacklist (CentOS, Red Hat)
def checkCentosBlacklist(package, blacklist):

    coincidences = False

    # Get the package dependencies
    packinfo = os.popen("repoquery --requires --resolve " + package).read()
    dependencies = packinfo.split('\n')


  # Check if the dependencies are in the blacklist
    for i in dependencies:
        for j in blacklist:
            if j in i:
                print("This dependence is not allowed " + i)
                coincidences = True

    # If there are dependencies in the blacklist, break the pipeline.
    if coincidences:
        sys.exit(1)


if __name__ == '__main__':

    # Check that exists the blacklist in blacklist.txt
    try:
        with open('/etc/package-controller/blacklist.txt','r') as bl:
            blacklist = bl.read().splitlines()
    except IOError:
        print "blacklist.txt not found"
        sys.exit(1)

    # Check the OS
    try:
        me = whereIAm().upper()
    except:
        print 'Are you in Debian/Ubuntu, OpenSUSE/SLES or CentOS/RHEL?'
        sys.exit(1)

    # Exectues a procedure depending of the OS
    try:
        if  'DEBIAN' in me or 'UBUNTU' in me:
                checkDebianBlacklist(sys.argv[1],blacklist)
        elif 'SUSE' in me:
                checkSUSEBlacklist(sys.argv[1],blacklist)
        elif 'CENTOS' or 'RHEL' in me:
                checkCentosBlacklist(sys.argv[1],blacklist)
    except IndexError:
        print 'Please specifies the package'
        sys.exit(1)
