#!/bin/bash

CONFIG_FILE="app_configuration.json"

# Check script parameters
if [[ ! $# == 3 ]]; then
    echo "[!] Error: The script needs three arguments: " 1>&2
    echo "    1) Image location. Example: 192.168.50.78:5000/bitnami/wordpress:latest" 1>&2
    echo "    2) Image name when pushed. Example:  /testimages/wordpress" 1>&2
    echo "    3) Image tag  when pushed. Examples: \"latest\", \"1.4\""   1>&2
    echo "Example usage: $0 192.168.50.78:5000/bitnami/wordpress:latest /testimages/wordpress latest" 1>&2
    exit 1
fi 

# Root is needed in order to run the commands
if [[ "$(id -u)" != "0" ]]; then
   echo "[!] Error: this script must be run as root!" 1>&2
   exit 1
fi

REGISTRY_ADDR=$(sed -n 's|.*"PRIVATE_REGISTRY_ADDR": "\([^"]*\)".*|\1|p' ${CONFIG_FILE})

IMAGE_LOCATION="$1"
IMAGE_NAME="$2"
IMAGE_TAG="$3"

IMAGE_PUSH="${REGISTRY_ADDR}/${IMAGE_NAME}:${IMAGE_TAG}"

# Pull the image
docker pull ${IMAGE_LOCATION}
if [[ ! $? = 0 ]]; then
    echo "[!] Error pulling the image \"${IMAGE_LOCATION}\"" 1>&2
    exit 1
fi
# Tag it in order to push it afterwards
docker image tag ${IMAGE_LOCATION} ${REGISTRY_ADDR}/${IMAGE_NAME}:${IMAGE_TAG}
if [[ ! $? = 0 ]]; then
    echo "[!] Error tagging the image \"${IMAGE_LOCATION}\" as \"${IMAGE_PUSH}\"" 1>&2
    exit 1
fi
# Push the image
docker image push ${IMAGE_PUSH}
if [[ ! $? = 0 ]]; then
    echo "[!] Error pushing the image \"${IMAGE_PUSH}\"" 1>&2
    exit 1
fi

echo "Image pushed successfully!"
exit 0

