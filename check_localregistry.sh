#!/bin/bash

# Check if docker is installed, install it if not
if [[ "$(command -v docker)" ]]; then
    echo "Docker is installed"
else
    zypper in –y ${package}
    if [[ ! $? ]]; then
        echo "Error installing docker, exiting..."
        exit 1
    else
        echo "Successfully installed docker"
    fi
fi

# Enable on startup and start it
systemctl enable docker
systemctl start docker

# Check for local docker registry
NAME='localregistry'

if [[ ! $(docker ps --filter "name=^/${NAME}$" --format '{{.Names}}') == ${NAME} ]]; then
    echo "Starting local registry..."
    if [[ $(docker ps -a --filter "name=^/${NAME}$" --format '{{.Names}}') == ${NAME} ]]; then
        docker start ${NAME}
    else
        docker run -d -p 5000:5000 --name ${NAME} registry:2
    fi
fi