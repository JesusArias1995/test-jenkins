#!/usr/bin/python

import sys,getopt,os

# Get the route of the Dockerfile
def where_dockerfile(argv):
    dockerfile_path = ''

    try:
        opts, args = getopt.getopt(argv, "-f")
    except getopt.GetoptError:
        print('app.py -f <route to Dockerfile>')
        sys.exit(1)

    for opt, arg in opts:
        if opt == '-h':
            print('app.py -f <route to Dockerfile>')
            sys.exit(1)
        elif opt == "-f":
            try:
                dockerfile_path = args[0]
            except IndexError:
                print('app.py -f <route to Dockerfile>')
                sys.exit(1)
    return dockerfile_path


# Search scripts in the Dockerfile
def read_script(dockerfile_path):
    with open(dockerfile_path+"Dockerfile") as dockerfile:
        content = dockerfile.readlines()
    for line in content:
        if line.strip("\n").endswith(".sh"):
            route = line.split(' ')[-1].strip("\n")
            check_script(route)

# Dont allow to install package from the script
def check_script(route):
    with open(route) as script:
        content = script.readlines()
    for line in content:
        if 'apt' in line:
            print('Is forbidden use apt in scripts')
            sys.exit(1)
        elif 'apt-get' in line:
            print('Is forbidden use apt-get in scripts')
            sys.exit(1)
        elif 'zypper' in line:
            print('Is forbidden use zypper in scripts')
            sys.exit(1)
        elif 'yum' in line:
            print('Is forbidden use yum in scripts')
            sys.exit(1)
        elif 'dpkg' in line:
            print('Is forbidden use dpkg in scripts')
            sys.exit(1)
        elif 'rpm' in line:
            print('Is forbidden use rpm in scripts')
            sys.exit(1)
        elif 'make install' in line:
            print('Is forbidden use make install in scripts')
            sys.exit(1)

# Get the Dockerfile lines where executes RUN
def check_run(dockerfile_path):
    try:
        with open(dockerfile_path+"Dockerfile") as dockerfile:
            content = dockerfile.readlines()
    except IOError:
        print "Please indicate the Dockerfile like /root"
        sys.exit(1)
    for line in content:
        if line.startswith("RUN"):
            line = line.strip("\n")
            # If the line contains RUN, send it to check_line
            check_line(line)

# Get a list of package and check if the package is in the whitelist
def check_packages(package_list):
    for package in package_list:
        if package == '\\':
           index = package_list.index(package)
           del package_list[index]

    for package in package_list:
        # If the package is in the whitelist
        if package not in whitelist:
            print "{} not in whitelist".format(package)
            sys.exit(1)
        # Check that the dependencies of the package are not in the blacklist
        cmd = "./blacklist.py {}".format(package)
        test = os.system(cmd)
        if test != 0:
            sys.exit(1)

# Filter the line and just get the names of the packages, if there are a forbidden action, exit from the pipeline
def check_line(line):
    string_list = line.split(" ")
    if "RUN" in string_list:
        string_list.remove("RUN")

        # Is not allowed edit the repositories
        if "add-apt-repository" in string_list:
            print("Is forbidden use custom repositories")
            sys.exit(1)

        if "--add-repo" in string_list:
            print("Is forbidden use custom repositories")
            sys.exit(1)

        if "ar" in string_list:
            print("Is forbidden use custom repositories")
            sys.exit(1)

        if "echo" in string_list:
            try:
                try:
                    index = string_list.index(">")
                except ValueError:
                    index = string_list.index(">>")
            except ValueError:
                None

        if "index" in locals():
                if "yum" in string_list[index+1] or "apt" in string_list[index+1]:
                        print("You cant add non official repositories")
                        sys.exit(1)

        # Remove characters from the package manager
        if "apt-get" in string_list and "install" in string_list:
            string_list.remove("apt-get")
            string_list.remove("install")
            try:
                string_list.remove("-y")
            except ValueError:
                None
            check_packages(string_list)

        if "yum" in string_list and "install" in string_list:
            string_list.remove("yum")
            string_list.remove("install")
            try:
                string_list.remove("-y")
            except:
                None
            check_packages(string_list)

        if "apt" in string_list and "install" in string_list:
            string_list.remove("apt")
            string_list.remove("install")
            try:
                string_list.remove("-y")
            except:
                None
            check_packages(string_list)

        if "zypper" in string_list and ("install" in string_list or "in" in string_list):
            string_list.remove("zypper")
            try:
                string_list.remove("install")
            except:
                string_list.remove("in")
            try:
                string_list.remove("-y")
            except:
                None
            check_packages(string_list)

def main(argv):
    dockerfile_path=where_dockerfile(argv)
    check_run(dockerfile_path)
    read_script(dockerfile_path)

if __name__== "__main__":
    with open("/etc/package-controller/whitelist.txt") as content:
         filter = content.readlines()
    whitelist = []
    for elem in filter:
        whitelist.append(elem.split("\n")[0])
    main(sys.argv[1:])
